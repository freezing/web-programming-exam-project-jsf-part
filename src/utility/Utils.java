package utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import managed_beans.MessageBean;

public class Utils {
	public static int parseStatus(List<String> response) {
		for (String s : response) {
			if (s.contains("STATUS=")) {
				String subStr = s.substring(s.indexOf("STATUS=") + "STATUS=".length());
				System.out.println("Parsed status = " + subStr);
				return Integer.valueOf(subStr);
			}
		}
		System.out.println("Parsed status = -1");
		return -1;
	}
	
	public static void updateMessageBean(List<String> response, MessageBean message) {
		int status = parseStatus(response);
		
		String text = "";
		for (String s : response) {
			if (!s.startsWith("STATUS=")) {
				text += s.replaceAll("\\{~~\\}", " ");
			}
		}
		
		if (status != 0) {
			message.set(true, "error", text);
		}
		else {
			message.set(false);
		}
	}
	
	public static void logResponse(Logger logger, List<String> response) {
		String msg = "";
		for (String s : response) {
			msg += s;
		}
		logger.log(Level.INFO, msg);
	}
	
	public static String[] splitString(String line) {
		return line.split("\\{~\\}");
	}
	
	public static String joinString(String[] args, String joiner) {
		String result = "";
		for (int i = 0; i < args.length; i++) {
			result += args[i];
			if (i < args.length - 1) {
				result += joiner;
			}
		}
		return result;
	}

	public static Date getDate(String dateString) {
		String[] tmp = dateString.split("-");
		for (int i = 0; i < tmp.length; i++) {
			tmp[i] = tmp[i].trim();
		}

		int year = Integer.parseInt(tmp[0]);
		int month = Integer.parseInt(tmp[1]);
		int day = Integer.parseInt(tmp[2]);

		int hour = Integer.parseInt(tmp[3].split(":")[0]);
		int minute = Integer.parseInt(tmp[3].split(":")[1]);
		return new Date(year, month, day, hour, minute);
	}

	public static String dateToString(Date date) {
		return date.getYear() + "-" + date.getMonth() + "-" + date.getDay()
				+ "-" + date.getHours() + ":" + date.getMinutes();
	}

	public static List<String> removeStatus(List<String> response) {
		List<String> list = new ArrayList<String>();
		for (String s : response) {
			if (!s.contains("STATUS=")) {
				list.add(s);
			}
		}
		return list;
	}

	public static String[] splitStringSpaces(String s) {
		return s.split("\\{~~\\}");
	}
}
