package model;

import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import utility.Utils;

public class Flight {
	private static Logger logger = Logger.getLogger("Flight");
	
	private int redniBroj;
	private String id;
	private Date departureTime;
	private Date arrivalTime;
	private String source;
	private String destination;
	private int numberOfPassengers;
	
	public Flight(){}
	
	public Flight(String flightString) {
		logger.log(Level.INFO, flightString);
		
		String[] args = Utils.splitStringSpaces(flightString);
		logger.log(Level.INFO, Arrays.toString(args));
		
		id = args[0];
		departureTime = Utils.getDate(args[1]);
		arrivalTime = Utils.getDate(args[2]);
		source = args[3];
		destination = args[4];
	}

	public Flight(String id, Date departureTime, Date arrivalTime, String source,
			String destination, int numberOfPassengers) {
		this.id = id;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.source = source;
		this.destination = destination;
		this.numberOfPassengers = numberOfPassengers;
	}
	
	public int getRedniBroj() {
		return redniBroj;
	}
	
	public void setRedniBroj(int redniBroj) {
		this.redniBroj = redniBroj;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}
	
	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}
	
	@Override
	public String toString() {
		return id + "{~~}" + Utils.dateToString(departureTime) + "{~~}" 
			+ Utils.dateToString(arrivalTime) + "{~~}" + source + "{~~}" 
				+ destination + "{~~}" + numberOfPassengers;
	}
}
