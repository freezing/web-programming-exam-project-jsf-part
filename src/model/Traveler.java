package model;

import utility.Utils;

public class Traveler {
	private String jmbg;
	private String name;
	private String lastName;
	
	public Traveler(String jmbg, String name, String lastName) {
		this.jmbg = jmbg;
		this.name = name;
		this.lastName = lastName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbg == null) ? 0 : jmbg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Traveler other = (Traveler) obj;
		if (jmbg == null) {
			if (other.jmbg != null)
				return false;
		} else if (!jmbg.equals(other.jmbg))
			return false;
		return true;
	}

	public Traveler(String travelerString) {
		String[] args = Utils.splitStringSpaces(travelerString);
		jmbg = args[0];
		name = args[1];
		lastName = args[2];
	}
	
	public String getJmbg() {
		return jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return name + " " + lastName;
	}
	
	@Override
	public String toString() {
		return jmbg + "{~~}" + name + "{~~}" + lastName;
	}
	
	public static Traveler fromId(String id) {
		return new Traveler(id, "", "");
	}
}
