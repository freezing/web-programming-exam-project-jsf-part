package validators;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class DateValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent arg1, Object arg2)
			throws ValidatorException {
		String s = (String) arg2;
		
		String regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
		if (!Pattern.matches(regex, s)) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Format datuma nije dobar.");
			message.setDetail("Format datuma nije dobar. Format mora biti: 'yyyy-mm-dd'");
			context.addMessage("FlightForm:date", message);
			throw new ValidatorException(message);
		}
		else {
			String[] t = s.split("-");
			int yyyy = Integer.valueOf(t[0]);
			int mm = Integer.valueOf(t[1]);
			int dd = Integer.valueOf(t[2]);
			
			if (checkDate(yyyy, mm, dd)) {
				FacesMessage message = new FacesMessage();
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				message.setSummary("Uneti datum nije validan.");
				message.setDetail("Uneti datum nije validan. "
						+ "Format datuma mora biti oblika: 'yyyy:mm:dd' "
						+ "i datum mora biti postojeci!");
				context.addMessage("FlightForm:date", message);
				throw new ValidatorException(message);
			}
		}
	}

	private boolean checkDate(int yyyy, int mm, int dd) {
		return dd > 31 || mm > 12;
	}

}
