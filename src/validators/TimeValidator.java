package validators;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class TimeValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent arg1, Object arg2)
			throws ValidatorException {
		String s = (String) arg2;
		
		String regex = "[0-9][0-9]:[0-9][0-9]";
		if (!Pattern.matches(regex, s)) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Format vremena nije dobar.");
			message.setDetail("Format vremena nije dobar. Format mora biti oblika 'hh:mm'");
			context.addMessage("FlightForm:date", message);
			throw new ValidatorException(message);
		}
		else {
			String[] t = s.split(":");
			int hh = Integer.valueOf(t[0]);
			int mm = Integer.valueOf(t[1]);
			
			if (hh >= 24 || mm >= 60) {
				FacesMessage message = new FacesMessage();
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				message.setSummary("Uneto vreme nije validno.");
				message.setDetail("Uneto vreme nije validno. "
						+ "Format vremena mora biti oblika: 'hh:mm' "
						+ "gde je hh izmedju 0 i 23, a mm izmedju 0 i 59");
				context.addMessage("FlightForm:date", message);
				throw new ValidatorException(message);
			}
		}
	}

}
