package managed_beans;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import model.Flight;
import model.Traveler;
import utility.Utils;

@ManagedBean(name="travelerInfoBean")
@RequestScoped
public class TravelerInfoBean {
	private static final Logger logger = Logger.getLogger("TravelerInfoBean");
	
	@ManagedProperty("#{param.passengerId}")
	private String id;
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	private String fullname;
	
	private List<Flight> flights;
	
	@PostConstruct
	public void init() {
		System.out.println(fullname);
		mediator.sendCommand("LIST_PASSENGER", id);
		List<String> response = mediator.readResponse();
		
		Utils.logResponse(logger, response);
		
		int status = Utils.parseStatus(response);
		response = Utils.removeStatus(response);
		
		if (status == 0) {
			Traveler t = new Traveler(response.get(0));
			fullname = t.getFullName();
		}
		else {
			
		}
		
		mediator.sendCommand("LIST_PASSENGER_RESERVATIONS", id);
		response = mediator.readResponse();
		
		Utils.logResponse(logger, response);
		
		status = Utils.parseStatus(response);
		response = Utils.removeStatus(response);
		
		if (status == 0) {
			int redniBroj = 1;
			flights = new ArrayList<Flight>();
			for (String flightString : response) {
				Flight f = new Flight(flightString);
				f.setRedniBroj(redniBroj++);
				flights.add(f);
			}
		}
		else {
			
		}
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}	
	public List<Flight> getFlights() {
		return flights;
	}
}
