package managed_beans;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import model.Flight;
import utility.Utils;

@ManagedBean(name="reservationBean")
@RequestScoped
public class ReservationBean {
	private static final Logger logger = Logger.getLogger("ReservationBean");
	
	@ManagedProperty("#{param.flightId}")
	private String flightId;
	
	private String jmbg;
	private String name;
	private String lastName;
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	@ManagedProperty("#{messageBean}")
	private MessageBean message;
	
	public String reserve() {  
		System.out.println("FLIGHT ID = " + flightId);
		
		mediator.sendCommand("RESERVE", flightId, jmbg, name, lastName);
		List<String> response = mediator.readResponse();
		
		Utils.logResponse(logger, response);
		int status = Utils.parseStatus(response);
		logger.log(Level.INFO, "Status = " + status);
		
		if (status == 0) { 
			response = Utils.removeStatus(response);
			// Get flight info
			mediator.sendCommand("LIST", flightId);
			List<String> flightResponse = mediator.readResponse();
			Utils.logResponse(logger, flightResponse);
			
			flightResponse = Utils.removeStatus(flightResponse);
			Flight flight = new Flight(flightResponse.get(0));
			
			String text = "Sediste je rezervisano za let: " + flight.getId() + ", od: " + flight.getSource() + ", do: "
					+ flight.getDestination() + ", Polazak: " + Utils.dateToString(flight.getDepartureTime())
					+ ", Dolazak: " + Utils.dateToString(flight.getArrivalTime());
			
			text += ", za putnika: " + name + " " + lastName;
			message.set(true, "ReservationBeanMessage", text);
			return "success";
		}
		else {
			Utils.updateMessageBean(response, message);
			return "fail";
		}
	}
	
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
	
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	public String getJmbg() {
		return jmbg;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	public String getFlightId() {
		return flightId;
	}
	public void setMessage(MessageBean message) {
		this.message = message;
	}
}
