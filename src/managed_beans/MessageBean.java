package managed_beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="messageBean")
@SessionScoped
public class MessageBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String type;
	private String text;
	private boolean toShow;
	
	public String getText() {
		return text;
	}
	
	public void set(boolean toShow, String type, String text) {
		this.toShow = toShow;
		this.type = type;
		this.text = text;
	}
	
	public String getType() {
		return type;
	}
	
	public boolean toShow() {
		return toShow;
	}

	public void set(boolean toShow) {
		this.toShow = toShow;
	}
}
