package managed_beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import model.Flight;
import model.Traveler;
import utility.Utils;

@ManagedBean(name="flightInfoBean")
@RequestScoped
public class FlightInfoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("FlightInfoBean");

	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	@ManagedProperty("#{param.flightId}")
	private String flightId;
	
	private String flightInfo;
	
	private Collection<Traveler> reservations;
	
	public String getFlightInfo() {
		return flightInfo;
	}
	
	@PostConstruct
	public void init() {
		// Get flight info
		mediator.sendCommand("LIST", flightId);
		List<String> response = mediator.readResponse();
		response = Utils.removeStatus(response);
		Flight flight = new Flight(response.get(0));
		flightInfo = "Let " + flight.getId() + ", od: " + flight.getSource() + ", do: "
				+ flight.getDestination() + ", Polazak: " + Utils.dateToString(flight.getDepartureTime())
				+ ", Dolazak: " + Utils.dateToString(flight.getArrivalTime());
		
		logger.log(Level.INFO, flightInfo);
		
		// Get list of reservations
		mediator.sendCommand("LIST_RESERVATIONS", flightId);
		response = mediator.readResponse();
		
		Utils.logResponse(logger, response);
		
		response = Utils.removeStatus(response);
		reservations = new ArrayList<Traveler>();
		for (String s : response) {
			System.out.println("Traveler: " + s);
			reservations.add(new Traveler(s));
		}
	}
	
	public Collection<Traveler> getReservations() {
		return reservations;
	}
	
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	
	public String getFlightId() {
		return flightId;
	}
	
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
}
