package managed_beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import utility.Utils;

@ManagedBean(name="addFlightBean")
@ViewScoped
public class AddFlightBean implements Serializable {
	private static final Logger logger = Logger.getLogger("AddFlight");
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	private String flightId;
	private String departureDate;
	private String departureTime;
	private String arrivalDate;
	private String arrivalTime;
	private String source;
	private String destination;
	private String numberOfPassengers;
	
	@ManagedProperty("#{messageBean}")
	private MessageBean message;
	
	public String addFlight() {
		mediator.sendCommand("ADD", 
				flightId, 
				departureDate + "-" + departureTime,
				arrivalDate + "-" + arrivalTime,
				source,
				destination,
				numberOfPassengers
				);
		List<String> response = mediator.readResponse();
		Utils.logResponse(logger, response);
		
		int status = Utils.parseStatus(response);
		if (status == 0) {
			String text = "Dodat novi let: " + flightId
					+ ", od: " + source + ", do: " + destination
				    + ", Polazak: " + departureDate + ", u: " + departureTime
				    + ", Dolazak: " + arrivalDate + ", u: " + arrivalTime;
			message.set(true, "Added Flight Info", text);
			return "success";
		}
		else {
			return "fail";
		}
	}
	
	public String getFlightId() {
		return flightId;
	}
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getNumberOfPassengers() {
		return numberOfPassengers;
	}
	public void setNumberOfPassengers(String numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
	public void setMessage(MessageBean message) {
		this.message = message;
	}
}
