package managed_beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import utility.Utils;

@ManagedBean(name="userBean")
@SessionScoped
public class UserBean implements Serializable{
	private static final long serialVersionUID = 5113569074242717833L;
	private static Logger logger = Logger.getLogger("UserBean");
	
	private String username = null;
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void login(String user) {
		username = user;
	}
	public String logout() {
		mediator.sendCommand("LOGOFF");
		
		List<String> response = mediator.readResponse();
		Utils.logResponse(logger, response);
		
		username = null;
		return "logged out";
	}
	public boolean isLoggedIn(){
		return username != null;
	}

	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
}
