package managed_beans;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="mediatorBean")
@SessionScoped
public class MediatorBean implements Serializable {
	private static final Logger logger = Logger.getLogger("Mediator");
	private static final long serialVersionUID = 1L;
	
	private static final String IP_ADDRESS = "127.0.0.1";
	private static final int PORT = 12345;

	private BufferedReader in;
	private PrintWriter out;
	private Socket sock;
	
	@PostConstruct
	public void init() {
		try {
			InetAddress addr = InetAddress.getByName(IP_ADDRESS);
			sock = new Socket(addr, PORT);
			in = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					sock.getOutputStream())));
		} catch (Exception e) {
			System.err.println(e);
			System.exit(1);
		}
	}
	
	private void quit() {
		sendCommand("QUIT");
		System.out.println("Server is successfully closed!");
		try {
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCommand(String command, String ... args) {
		String msg = command;
		out.print(command);
		for (String param : args) {
			out.print("{~}" + param);
			msg += "{~}" + param;
		}
		logger.log(Level.INFO, msg);
		out.println();
		out.flush();
	}
	
	public void sendCommand(String command) {
		logger.log(Level.INFO, command);
		out.println(command);
		out.flush();
	}
	
	public List<String> readResponse() {
		String line = readLine();	// Skip START
		
		List<String> list = new LinkedList<String>();
		while (true) {
			line = readLine();
			
			if (line.equalsIgnoreCase("end")) {
				break;
			}
			line = line.trim();
			String[] lines = line.split("\\{~\\}");
			for (String s : lines) {
				if (s.length() > 0) {
					list.add(s);
				}
			}
		}
		
		return list;
	}
	
	public String readLine() {
		try {
			return in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@PreDestroy
	public void destructor(){
		try {
			quit();
			in.close();
			out.close();
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
