package managed_beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import model.Flight;
import model.Traveler;
import utility.Utils;

@ManagedBean(name="potvrdiOtkazBean")
@RequestScoped
public class PotvrdiOtkazBean {
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	@ManagedProperty("#{param.passengerId}")
	private String passengerId;
	
	@ManagedProperty("#{param.flightId}")
	private String flightId;
	
	private String info;
	private String flightInfo;
	private String travelerInfo;
	
	@PostConstruct
	public void init() {
		System.out.println("POTVRDI| FLIGHT ID = " + flightId);
		System.out.println("POTVRDI| PASSENGER ID = " + passengerId);
		
		mediator.sendCommand("LIST", flightId);
		List<String> response = mediator.readResponse();
		response = Utils.removeStatus(response);
		Flight flight = new Flight(response.get(0));
		
		flightInfo = "Let " + flight.getId() + ", od: " + flight.getSource() + ", do: "
				+ flight.getDestination() + ", Polazak: " + Utils.dateToString(flight.getDepartureTime())
				+ ", Dolazak: " + Utils.dateToString(flight.getArrivalTime());
		
		mediator.sendCommand("LIST_PASSENGER", passengerId);
		response = mediator.readResponse();
		
		int status = Utils.parseStatus(response);
		response = Utils.removeStatus(response);
		
		if (status == 0) {
			Traveler t = new Traveler(response.get(0));
			travelerInfo = t.getFullName();
			
			info = "Otkaz rezervacije sedista za let: " + flightInfo + ", za putnika: " + travelerInfo;
		}
		else {
			
		}
	}
	
	public String getInfo() {
		return info;
	}
	
	public String getPassengerId() {
		return passengerId;
	}
	
	public String getFlightId() {
		return flightId;
	}
	
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
	
	public String otkazi() {
		mediator.sendCommand("CANCEL", flightId, passengerId);
		List<String> response = mediator.readResponse();
		
		int status = Utils.parseStatus(response);
		
		if (status == 0) {
			List<String> text = new ArrayList<String>();
			text.add("Sediste na letu: " + flightInfo + ", za putnika: " + travelerInfo + " OTKAZANO!");
			Utils.updateMessageBean(text, message);
		}
		else {
			Utils.updateMessageBean(response, message);
		}
		
		// U oba slucaja (status = 0 ili status != 0) se ide pokazuje ista stranica
		return "success";
	}
	
	@ManagedProperty("#{messageBean}")
	private MessageBean message;
	
	public void setMessage(MessageBean message) {
		this.message = message;
	}
	
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	
	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}
}
