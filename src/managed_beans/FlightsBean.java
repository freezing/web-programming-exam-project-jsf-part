package managed_beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import model.Flight;
import utility.Utils;

@ManagedBean(name="f")
@RequestScoped
public class FlightsBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("FlightsBean");
	
	private List<Flight> flights;
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	@PostConstruct
	public void init(){
		mediator.sendCommand("LIST_ALL");
		List<String> response = Utils.removeStatus(mediator.readResponse());
		Utils.logResponse(logger, response);
		
		flights = new ArrayList<Flight>();
		int redniBroj = 1;
		for (String flightString : response) {
			Flight flight = new Flight(flightString);
			flight.setRedniBroj(redniBroj++);
			flights.add(flight);
		}
	}
	
	public Collection<Flight> getFlights() {
		return flights;
	}
	
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
	
	public MediatorBean getMediator() {
		return mediator;
	}
}
