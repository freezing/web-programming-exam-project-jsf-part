package managed_beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import utility.Utils;

@ManagedBean(name="loginBean")
@RequestScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("LoginBean");
	
	@ManagedProperty("#{mediatorBean}")
	private MediatorBean mediator;
	
	@ManagedProperty("#{userBean}")
	private UserBean userBean;
	
	@ManagedProperty("#{messageBean}")
	private MessageBean message;

	public LoginBean() {}
	
	private String username;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String login() {
		mediator.sendCommand("LOGIN", username);
		List<String> response = mediator.readResponse();
		Utils.logResponse(logger, response);
		
		int status = Utils.parseStatus(response);

		if (status == 0) {
			userBean.setUsername(username);
			return "success";
		}
		else {
			Utils.updateMessageBean(response, message);
			return "fail";
		}
	}
	
	public void setMediator(MediatorBean mediator) {
		this.mediator = mediator;
	}
	
	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}
	
	public void setMessage(MessageBean message) {
		this.message = message;
	}
}
